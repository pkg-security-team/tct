#!/bin/sh

# md5_image_maker: Produce raw images of partitions and store their appropriate
# md5 sums in a separate file.
# 2001-2002 Yotam Rubin <yotam@makif.omer.k12.il>
#
# Usage: md5_image_maker <partition> <partition> ...

MD5SUM="/usr/bin/md5sum"

if [ -z "${1}" ]
then
	echo "Usage: ${0} PARTITION [PARTITON ...]"
	exit 1
fi

touch md5sums

for PARTITION in ${@}
do
	dd if=/dev/${PARTITION} of=${PARTITION}.bin
	${MD5SUM} ${PARTITION}.bin >> md5sums
done
